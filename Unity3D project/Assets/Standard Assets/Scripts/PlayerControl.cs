﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.UI;

public class PlayerControl : MonoBehaviour {
	
	public float speed = 5;

	private Animator animator;

	public GameObject proyectil;
	public int orientation = 2;
	public int last_orientantion = 0;


	public bool muerto = false;
	private float time;
	public bool pausa;

	//Variables utilizadas para el texto de combate
	//flotante. 
	public GameObject combatText;
	public GameObject healingText;

	//Variables para controlar la vida del personaje
	public int vida;
	public int maxvida = 10;

	//Variables para modificar la GUI desde este script.
	public Scrollbar barra_vida;
	public Text tiempo_vivo;




	void Start(){
		animator = GetComponent<Animator>();
		muerto = false;
		time = 0.0f;
		pausa = false;
		maxvida = 10;
		vida = 10;
	}

	void Update (){
		
		tiempo_vivo.text = "Tiempo vivo: " + Mathf.Round(Time.timeSinceLevelLoad).ToString() + "(s)";

		if(!muerto && !pausa){
			if(CrossPlatformInputManager.GetAxis("Horizontal")>0 && Mathf.Abs(CrossPlatformInputManager.GetAxis("Horizontal"))>=Mathf.Abs(CrossPlatformInputManager.GetAxis("Vertical")))
				Movimiento(2);
			else if(CrossPlatformInputManager.GetAxis("Horizontal")<0 && Mathf.Abs(CrossPlatformInputManager.GetAxis("Horizontal"))>=Mathf.Abs(CrossPlatformInputManager.GetAxis("Vertical")))
				Movimiento(4);
			else if(CrossPlatformInputManager.GetAxis("Vertical")>0 && Mathf.Abs(CrossPlatformInputManager.GetAxis("Horizontal"))<Mathf.Abs(CrossPlatformInputManager.GetAxis("Vertical")))
				Movimiento(1);
			else if(CrossPlatformInputManager.GetAxis("Vertical")<0 && Mathf.Abs(CrossPlatformInputManager.GetAxis("Horizontal"))<Mathf.Abs(CrossPlatformInputManager.GetAxis("Vertical")))
				Movimiento(3);
			else
				Movimiento(0);
			if(CrossPlatformInputManager.GetButton("Fire"))
				BolaDeFuego();
			UpdateAnimestate();
		}

	}
	public void Pausar (){
		//Para pausar se cambia la escala de tiempo a 0. Se almacena la escala actual
		//en la variable time
		if(!muerto){
			float temp = time;
			time = Time.timeScale;
			Time.timeScale = temp;
			pausa = !pausa;
			//Se desactiva el audiolistener, así no se oirá nada.
			AudioListener.pause = !AudioListener.pause;
		}
	}

	public void Reiniciar (){
		//Se recarga el nivel
		Application.LoadLevel(Application.loadedLevel);
		if(pausa)
			Pausar();
	}

	public void Salir (){
		Application.Quit();
	}

	public void Morir (){
		//Se activa el modo kinematico del rigidbody del personaje
		GetComponent<Rigidbody2D>().isKinematic = true;
		Pausar();
		muerto = true;
	}

	public void  BolaDeFuego (){
		if(!muerto && !pausa){
			Vector3 posicion = transform.position;
			switch(orientation){
			case 1:
				posicion.y += 0.3f;
				break;
			case 2:
				posicion.x += 0.3f;
				break;
			case 3:
				posicion.y -=0.2f;
				break;
			case 4:
				posicion.x -=0.3f;
				break;
			}


			Instantiate(proyectil, posicion, transform.rotation);
		}

	}

	public void Movimiento (int direccion){


		if (direccion == 1){
			last_orientantion = orientation;
			orientation = 1;
		}
		else if (direccion == 3){
			last_orientantion = orientation;
			orientation = 3;
		}
		else if (direccion == 4){
			last_orientantion = orientation;
			orientation = 4;
		}
		else if (direccion == 2){
			last_orientantion = orientation;
			orientation = 2;
		}
		else{
			orientation = 0;
		}
		GetComponent<Rigidbody2D>().velocity = new Vector2(speed*CrossPlatformInputManager.GetAxis("Horizontal"),speed*CrossPlatformInputManager.GetAxis("Vertical"));

	}
	public void UpdateAnimestate (){
	animator.SetInteger("orientation",orientation);
	}

	//Funcion para aplicar daño al personaje
	public void  ApplyDamage ( int damage  ){
		//Se resta la vida al personaje.
		vida = vida - damage;
		//Se muestra el daño recibido con texto flotante
		SpawnCombatText(-damage);
		//Se hace esto para realizar un casting de entero a real en UnityScript(Javascript)
		//Después se modifica la longitud de la barra de vida
		float perc_vida = (float)vida/maxvida;
		barra_vida.size = 1.0f - perc_vida;
		//Se comprueba si después de recibir el daño ha muerto
		CheckMuerte();

	}
	public void  CheckMuerte (){
		if(vida <= 0){
			Morir();
		}
	}
	//Función que define el daño que realiza una bola de fuego 
	//lanzada por el personaje
	public float Hechizo_bola_de_fuego (){
		return Random.Range(2,4);
	}


	public void  Heal ( int amount  ){
		//Se suma la curación a la vida actual, comprobando que
		//la nueva vida actual no supere a la vida máxima
		if(vida + amount > maxvida)
			vida = maxvida;
		else
			vida = vida + amount;
		//Se muestra la curación recibida con texto flotante	
		SpawnCombatText(amount); 
		//Después se modifica la longitud de la barra de vida
		float perc_vida = (float)vida/maxvida;
		barra_vida.size = 1.0f - perc_vida;

	}

	public void  SpawnCombatText ( float points){
		GameObject cb_text;
		//Dependiendo de si es daño o curación se instancia un texto distinto 
		if(points>0){
			cb_text = (GameObject)Instantiate(healingText,transform.position,transform.rotation);
		}
		else{
			cb_text = (GameObject)Instantiate(combatText,transform.position,transform.rotation);
		}

		cb_text.transform.SetParent(GameObject.FindGameObjectWithTag("MainCanvas").transform);
		cb_text.GetComponent<RectTransform>().localScale = Vector3.one;
		cb_text.GetComponent<Text>().text = points.ToString();
		Destroy(cb_text,1.0f);
	}



}