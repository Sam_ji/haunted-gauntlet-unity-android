﻿using UnityEngine;
using System.Collections;

public class Health_potion : MonoBehaviour {


public int power;
public AudioClip sound;


	void  OnCollisionEnter2D ( Collision2D coll  ){
	if(coll.gameObject.tag == "Player"){
		coll.gameObject.SendMessage("Heal", power);
		AudioSource.PlayClipAtPoint(sound, transform.position);
		Destroy(gameObject);
		}
}

}