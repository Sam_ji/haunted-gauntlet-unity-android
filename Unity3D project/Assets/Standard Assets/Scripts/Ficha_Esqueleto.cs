﻿

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Ficha_Esqueleto : MonoBehaviour {


public Scrollbar barra_vida;
public int percepcion_base = 2;
public float percepcion_habilidad = 0.3f; //Un 30%
public float fuerza_habilidad = 0.2f; //un 20%
public int dmg_arma = 2 ;


public GameObject combat_text;

public int vida;
public int maxvida;




	public void  ApplyDamage ( int damage  ){
	vida = vida - damage;
	Vector3 v = Camera.main.WorldToViewportPoint(transform.position);
	SpawnPts(-damage);
	SendMessage("Activate");
	float perc_vida = (vida+0.0f)/maxvida;
	barra_vida.size = 1.0f - perc_vida;
	CheckMuerte();

}


	public void  CheckMuerte (){
	if(vida<=0){
	SendMessage("Drop");
	Destroy(gameObject);
	Destroy(GetComponent<Rigidbody2D>());
	Destroy(GetComponent<IA_Esqueleto>());
	Destroy(this);
	}
}
		

	public float Percepcion (){
	float resultado;
	resultado = percepcion_base+ percepcion_base *(Random.Range(0.0f, 1.0f) + percepcion_habilidad);
	return resultado;
	
	
}


public float Danio (){
		return Random.Range(1,dmg_arma);

}

	public void  SpawnPts ( float points){
		GameObject cb_text = (GameObject)Instantiate(combat_text,transform.position,Quaternion.identity);
		cb_text.transform.SetParent(GetComponentInChildren<Canvas>().transform);
		cb_text.GetComponent<RectTransform>().localScale = Vector3.one;
		cb_text.GetComponent<Text>().text = points.ToString();
		Destroy(cb_text,1.0f);
}
}