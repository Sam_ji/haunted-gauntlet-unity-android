﻿using UnityEngine;
using System.Collections;

public class IA_Esqueleto : MonoBehaviour {



//Variables para controlar el movimiento y la animación
Animator animator;
public int orientation = 1;
public float speed = 3;
public int animestate = 0;
public int sig_mov = 0;

//Variable de personaje objetivo 

public GameObject personaje;


//Variables para esquivar colisiones
public bool  col_entorno;
public int eje_col;
public int sig_mov_obstaculo;

//Variable para distancia de agro
public float agro_distance = 3.0f;

//Variables de comportamiento
public bool  activado = false;
public bool  melee_player = false;

//Variables de ataque
public float ataque_cd;
public float next_atack;

//Variables de efectos de sonido
public AudioClip sound_attack;



void  Start (){
	//Obtenemos el animator asociado a este objeto
		animator = GetComponent<Animator>();
	//Obtenemos el personaje
	personaje = GameObject.FindGameObjectWithTag("Player");
	ataque_cd = 1;
	next_atack = 0;
	
	
	//Tirada de percepcion
	agro_distance = gameObject.GetComponent<Ficha_Esqueleto>().Percepcion();
	
	
	
}

void  Update (){
	CalcularRuta();
	Movimiento();
	Try_Attack();
	
	
}

void  Try_Attack (){
	if(Time.time> next_atack && melee_player){
	next_atack = Time.time + ataque_cd;
	float dmg = gameObject.GetComponent<Ficha_Esqueleto>().Danio();
	personaje.SendMessage("ApplyDamage", dmg);
	GetComponent<AudioSource>().PlayOneShot(sound_attack, 0.4f);
	}
}

void  Movimiento (){
	switch(sig_mov){
	case 1:
		orientation = 1;
		animestate = 1;
		GetComponent<Rigidbody2D>().velocity = new Vector2(0,speed);
		break;
	case 2:
		orientation = 2;
		animestate = 2;
		GetComponent<Rigidbody2D>().velocity = new Vector2(speed,0);
		break;
	case 3:
		orientation = 3;
		animestate = 3;
		GetComponent<Rigidbody2D>().velocity = new Vector2(0,-speed);
		break;
	case 4:
		orientation = 4;
		animestate = 4;
		GetComponent<Rigidbody2D>().velocity = new Vector2(-speed,0);
		break;
	
	case 0:
		animestate =0;
		GetComponent<Rigidbody2D>().velocity = Vector2.zero;
		break;
	}
	
	UpdateAnimestate();		
}

void  UpdateAnimestate (){
		switch(animestate){
		
		case 1:
			animator.SetBool("Arriba", true);
			animator.SetBool("Derecha", false);
			animator.SetBool("Abajo", false);
			animator.SetBool("Izquierda", false);
			animator.SetBool("Parado", false);
			break;
		case 2:
			animator.SetBool("Arriba", false);
			animator.SetBool("Derecha", true);
			animator.SetBool("Abajo", false);
			animator.SetBool("Izquierda", false);
			animator.SetBool("Parado", false);
			break;
		case 3:
			animator.SetBool("Arriba", false);
			animator.SetBool("Derecha", false);
			animator.SetBool("Abajo", true);
			animator.SetBool("Izquierda", false);
			animator.SetBool("Parado", false);
			break;
		case 4:
			animator.SetBool("Arriba", false);
			animator.SetBool("Derecha", false);
			animator.SetBool("Abajo", false);
			animator.SetBool("Izquierda", true);
			animator.SetBool("Parado", false);
			break;
		case 0: 
			animator.SetBool("Arriba", false);
			animator.SetBool("Derecha", false);
			animator.SetBool("Abajo", false);
			animator.SetBool("Izquierda", false);
			animator.SetBool("Parado", true);
			break;
			}
		
	}

void  OnCollisionEnter2D ( Collision2D coll  ){
	if(coll.gameObject.tag == "Player")
		melee_player = true;
	if(coll.gameObject.tag == "Arbol" || coll.gameObject.tag == "Fogata"){
		if(!col_entorno){
			eje_col = orientation;
			EsquivarObstaculo();
			}
		col_entorno = true;	
	}
	if(coll.gameObject.tag == "Esqueleto" && activado)
		coll.gameObject.SendMessage("Activate");

}

void  OnCollisionExit2D ( Collision2D coll  ){
	if(coll.gameObject.tag == "Player")
		melee_player = false;
	if(coll.gameObject.tag == "Arbol" || coll.gameObject.tag == "Fogata")
		col_entorno = false;
	if(coll.gameObject.tag == "Esqueleto" && activado)
		coll.gameObject.SendMessage("Activate");

}

void  CalcularRuta (){

	if((Vector3.Distance(personaje.transform.position, transform.position)<= agro_distance || activado) && !melee_player){
		activado = true;
		if(Mathf.Abs(personaje.transform.position.x - transform.position.x) > Mathf.Abs(personaje.transform.position.y - transform.position.y)){
			if(personaje.transform.position.x > transform.position.x)
				sig_mov = 2;
			else if(personaje.transform.position.x < transform.position.x)
				sig_mov = 4;
			}
		else{
		 	if(personaje.transform.position.y > transform.position.y)
				sig_mov = 1;
			else if(personaje.transform.position.y < transform.position.y)
				sig_mov = 3;
			}
	}
	else{
		sig_mov = 0;
	}
	if(col_entorno){
		sig_mov = sig_mov_obstaculo;
	}
}


void  EsquivarObstaculo (){
	if(eje_col == 1 || eje_col == 3){
		if(personaje.transform.position.x >= transform.position.x)
				sig_mov_obstaculo = 2;
		else if(personaje.transform.position.x < transform.position.x)
				sig_mov_obstaculo = 4;
	}
	else if (eje_col == 2 || eje_col == 4){
		if(personaje.transform.position.y >= transform.position.y)
				sig_mov_obstaculo = 1;
		else if(personaje.transform.position.y < transform.position.y)
				sig_mov_obstaculo = 3;
	}
}

void  Activate (){
 activado = true;
}
}