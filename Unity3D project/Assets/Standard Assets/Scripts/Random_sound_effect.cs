﻿using UnityEngine;
using System.Collections;
public class Random_sound_effect : MonoBehaviour {

public GameObject woman_scream;
public GameObject evil_laugh;
public float ran_timer;
public bool  r_lock;

void  Start (){
	//Así no se produce un efecto de sonido al comenzar la escena
	r_lock = true;
	Invoke("Open_lock", Random.Range(5.0f,10.0f));
}

void  Update (){
	Sound_effects();
}

public void  Ran_sound (){
	float x = Random.Range(0.0f,1.0f);
		GameObject s = new GameObject();
	if(x>0.1f && x< 0.3f)
		s = (GameObject)Instantiate(woman_scream, transform.position, transform.rotation);
	if(x>0.3f && x<0.6f)
		s = (GameObject)Instantiate(evil_laugh, transform.position, transform.rotation);
}

public void  Sound_effects (){
	if(!r_lock){
		Ran_sound();
		r_lock = true;
		Invoke("Open_lock", ran_timer);
	}

}

public void  Open_lock (){
	r_lock = false;
}
} 
