﻿
using UnityEngine;
using System.Collections;

public class Movimiento_FB : MonoBehaviour {



public float velocidad = 10;
private Animator animator;
private GameObject lanzador;


public void  Start (){
	lanzador = GameObject.FindGameObjectWithTag("Player");
	animator = GetComponent<Animator>();
	
		switch(lanzador.GetComponent<PlayerControl>().last_orientantion){
			case 1:
				GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f,velocidad);
				animator.Play("Arriba");
				break;
			case 2:
				GetComponent<Rigidbody2D>().velocity = new Vector2(velocidad,0.0f);
				animator.Play("Derecha"); 
				break;
			case 3:
				GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f,-velocidad);
				animator.Play("Abajo");
				break;
			case 4:
				GetComponent<Rigidbody2D>().velocity = new Vector2(-velocidad,0.0f);
				animator.Play("Izquierda");
				break;
					}
}
		

void  OnCollisionEnter2D ( Collision2D coll  ){
	if(coll.collider.gameObject.tag == "Esqueleto"){
		coll.collider.SendMessage("ApplyDamage", lanzador.GetComponent<PlayerControl>().Hechizo_bola_de_fuego());
	}
	if(coll.collider.gameObject.tag != "Player")
		Destroy(gameObject);

}


}