﻿

using UnityEngine;
using System.Collections;

public class Spawn : MonoBehaviour {
	public GameObject esqueleto;
	public GameObject esqueleto_elite;
	public bool cooldown;
	public float time;
	public float horizontal_bound_min;
	public float vertical_bound_min;
	public float horizontal_bound_max;
	public float vertical_bound_max;

	void  Start (){
		cooldown = false;
		Random_position();
		
	}

	void Update (){
		if(!cooldown){
			Invoke("invocar_esqueleto", time);
			cooldown = true;
		}
	}

	public void invocar_esqueleto (){
		Vector3 posicion;
		posicion.x = transform.position.x;
		posicion.y = transform.position.y;
		posicion.z = 1;
		int spawn_lottery = Random.Range(0,10);
		if(spawn_lottery<8){
			GameObject n_esqueleto= (GameObject)Instantiate(esqueleto, posicion, transform.rotation);
		}
		else
		{
			GameObject n_esqueleto_elite = (GameObject)Instantiate(esqueleto_elite, posicion, transform.rotation);
		}
		cooldown = false;
		Random_position();

	}
		
	public void  Random_position (){
		float x = Random.Range(horizontal_bound_min, horizontal_bound_max);
		float y = Random.Range(vertical_bound_min, vertical_bound_max);
		transform.position = new Vector3 (x,y,0.0f);
	}

	void  OnCollisionEnter2D (Collision2D coll){
		Random_position();
	}
}